module.exports = {
  "globDirectory": "./",
  "globPatterns": [
    "**/*.{png,html}"
  ],
  "swDest": "./service-worker.js",
  "swSrc": "./service-worker-entry-point.js"
};