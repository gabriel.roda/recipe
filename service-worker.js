'use strict';

importScripts('https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js');

self.addEventListener('install', (evt) => {
    self.skipWaiting();
});

self.addEventListener('activate', (evt) => {
    self.clients.claim();
});

workbox.precaching.precacheAndRoute([
  {
    "url": "404.html",
    "revision": "0a27a4163254fc8fce870c8cc3a3f94f"
  },
  {
    "url": "img/favicon.png",
    "revision": "c717d396432cb1d75cf9174452ef22a3"
  },
  {
    "url": "img/i-128.png",
    "revision": "57aa7769f6f623799193b9577d0e399c"
  },
  {
    "url": "img/i-144.png",
    "revision": "bf2ab90d86da539590b28bf031c6b1d4"
  },
  {
    "url": "img/i-152.png",
    "revision": "ac9f72f8e125a00b5a0748125a9f7591"
  },
  {
    "url": "img/i-192.png",
    "revision": "9ea1ac56a0a89b6b673e6b25e3b6d68f"
  },
  {
    "url": "img/i-256.png",
    "revision": "647dd1463ae1305fd02e58b1e7cebcfa"
  },
  {
    "url": "img/i-512.png",
    "revision": "3aec4f1452b69fbc17d3ba17c24c49f9"
  },
  {
    "url": "img/icon.png",
    "revision": "1f8be307f28a28f2da29283a1b152b73"
  },
  {
    "url": "index.html",
    "revision": "243ef96211564fe9ebdc66b69a81a935"
  }
]);

workbox.routing.registerRoute(
    /\.js$/,
    new workbox.strategies.StaleWhileRevalidate()
);

workbox.routing.registerRoute(
    // Cache CSS files.
    /\.css$/,
    // Use cache but update in the background.
    new workbox.strategies.StaleWhileRevalidate({
        // Use a custom cache name.
        cacheName: 'css-cache',
    })
);

workbox.routing.registerRoute(
    // Cache image files.
    /\.(?:png|jpg|jpeg|svg|gif)$/,
    // Use the cache if it's available.
    new workbox.strategies.CacheFirst({
        // Use a custom cache name.
        cacheName: 'image-cache',
        plugins: [
            new workbox.expiration.Plugin({
                // Cache only 20 images.
                maxEntries: 20,
                // Cache for a maximum of a week.
                maxAgeSeconds: 7 * 24 * 60 * 60,
            })
        ],
    })
);