let fs = firebase.firestore();
let storeRef = firebase.storage().ref();

function loadInfo(index) {
    userRef.collection('recipes').doc(index).get().then(doc => {
        document.querySelector('.pop-img').src = doc.data().imagem;

        document.querySelector('.pop-title').innerHTML = doc.data().nome;

        document.querySelector('.pop-ingredientes').innerHTML = "<h4>Ingredientes:</h4>";
        doc.data().ingredientes.forEach(ing => {
            let newIng = document.createElement('li');
            newIng.classList.add('pop-ingrediente');
            newIng.setAttribute('onclick', 'toggleComplete(this)');
            newIng.innerHTML = ing;

            document.querySelector('.pop-ingredientes').appendChild(newIng);
        })

        document.querySelector('.pop-preparo').innerHTML = "<h4>Modo de Preparo:</h4>";
        doc.data().preparo.forEach(prep => {
            let newPrep = document.createElement('li');
            newPrep.classList.add('pop-ingrediente');
            newPrep.setAttribute('onclick', 'toggleComplete(this)');
            newPrep.innerHTML = prep;

            document.querySelector('.pop-preparo').appendChild(newPrep);
        })

        document.querySelector('.popup').classList.toggle('active');
        document.body.classList.toggle('popup-open')
    })
}

function toggleComplete(el) {
    el.classList.toggle('complete')
}

function closePopup() {
    document.querySelector('.popup').classList.toggle('active');
    document.body.classList.toggle('popup-open')
}

function toggleAddPannel() {
    document.querySelector('.add-pannel').classList.toggle('active');
    document.querySelector('.button-add').classList.toggle('hidden');
}

var ui = new firebaseui.auth.AuthUI(firebase.auth());
ui.start('.botao-login', {
    signInOptions: [
        firebase.auth.GoogleAuthProvider.PROVIDER_ID
    ]
});

let uid;
let userRef;

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        // User is signed in.
        uid = user.uid;
        userRef = fs.collection('users').doc(uid);

        //esconder login area
        document.querySelector('.login-area').style.display = 'none';

        //carregar infos do usuario
        document.querySelector('.user-button').innerHTML = `<img src="${firebase.auth().currentUser.photoURL}" alt="Foto perfil">`;

        //ref do firestore
        userRef.get().then(function (doc) {
            if (doc.exists) {
                //console.log("user data:", doc.data());

                //carrega receitas
                userRef.collection("recipes").onSnapshot(function (snapshot) {
                    snapshot.docChanges().forEach(function (change) {
                        if (change.type === "added") {
                            //console.log("New city: ", change.doc.data());

                            let el = document.createElement('div');
                            el.setAttribute('data-id', change.doc.id);
                            el.classList.add('content-card');
                            el.setAttribute('onclick', `loadInfo("${change.doc.id}")`);

                            let el_img = document.createElement('img');
                            el_img.classList.add('content-bg');
                            el_img.src = change.doc.data().imagem;

                            let el_h = document.createElement('h2');
                            el_h.innerHTML = change.doc.data().nome;

                            el.appendChild(el_img);
                            el.appendChild(el_h);

                            document.querySelector('.content').appendChild(el);
                        }
                        if (change.type === "modified") {
                            //console.log("Modified city: ", change.doc.data());
                        }
                        if (change.type === "removed") {
                            //console.log("Removed city: ", change.doc.data());
                        }
                    });
                });
            } else {
                // doc.data() will be undefined in this case
                //console.log("Primeiro login");

                userRef.set({
                    displayName: user.displayName
                })
            }
        }).catch(function (error) {
            //console.log("Error getting document:", error);
        });

        /*
        fetch('https://www.instagram.com/p/B0hUVI3FPpt/?utm_source=ig_web_copy_link').then((response)=>{
page = response;
//console.log("promise resolved");
})

page.text().then((a, b)=>{pagedom = new DOMParser().parseFromString(a, 'text/html')})

JSON.parse(pagedom.querySelector('script[type^="application/ld+json').innerText).caption
        */
    } else {
        // No user is signed in.
    }
});

function signOut() {
    firebase.auth().signOut().then(() => {
        window.location.reload();
    })
}

function addItem(el) {
    let newIt = document.createElement('div');
    newIt.classList.add('input-extra');

    let newInp = document.createElement('input');
    newInp.setAttribute('type', 'text');

    let newBut = document.createElement('button');
    newBut.setAttribute('onclick', "removeItem(this)");
    newBut.innerHTML = "&minus;"

    newIt.appendChild(newInp);
    newIt.appendChild(newBut);

    el.parentElement.insertBefore(newIt, el);
}

function removeItem(el) {
    let parentDiv = el.parentElement;
    let rootDiv = parentDiv.parentElement;

    rootDiv.removeChild(parentDiv);
}


function uploadImage(event) {
    let inputEl = event.target;
    let row = inputEl.parentElement;
    let fileName = inputEl.files[0].name;

    //pegar a ref do storage
    let imgRef = storeRef.child(`images/${fileName}`);

    var uploadTask = imgRef.put(inputEl.files[0]);

    uploadTask.on('state_changed', function (snapshot) {
        var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        //console.log('Upload is ' + progress + '% done');
    }, function (error) {
        // Handle unsuccessful uploads
    }, function () {
        // Handle successful uploads on complete
        uploadTask.snapshot.ref.getDownloadURL().then(function (downloadURL) {
            //console.log('File available at', downloadURL);
            //trocar para a div com o nome e o botao de remover
            let divNome = document.createElement('div');
            divNome.classList.add('imagem-storage');

            let aNome = document.createElement('a');
            aNome.innerHTML = fileName;
            aNome.setAttribute('target', '_blank');
            aNome.setAttribute('href', downloadURL);

            let bNome = document.createElement('button');
            bNome.setAttribute('onclick', 'deleteImage(this)');
            bNome.innerHTML = 'Remover';

            divNome.appendChild(aNome);
            divNome.appendChild(bNome);

            row.innerHTML = '';
            row.appendChild(divNome);
        });

    });
}

function deleteImage(buttonEl) {
    //montar a ref da imagem
    let row = buttonEl.parentElement;
    let imgName = row.firstElementChild.innerHTML;
    let imgRef = storeRef.child(`images/${imgName}`);

    // Delete the file
    imgRef.delete().then(function () {
        // File deleted successfully
        //console.log("imagem removida.")

        //colocar o input de volta
        let fileI = document.createElement('input');
        fileI.setAttribute('type', 'file');
        fileI.setAttribute('accept', 'image/*');
        fileI.setAttribute('onchange', 'uploadImage(event)');
        fileI.setAttribute('name', 'file');
        fileI.setAttribute('id', 'file');
        fileI.classList.add('inputfile');

        let labelI = document.createElement('label');
        labelI.setAttribute('for', 'file');
        labelI.innerHTML = "Escolher imagem";

        row = row.parentElement;

        row.removeChild(row.lastElementChild);
        row.appendChild(fileI);
        row.appendChild(labelI);

    }).catch(function (error) {
        // Uh-oh, an error occurred!
        //console.log(error);
    });
}

function saveNewRecipe() {
    let newRecipe = {};

    // imagem
    let imgUrl;
    try {
        imgUrl = document.querySelector('.imagem-storage a').href;
    } catch (e) {
        imgUrl = "img/ia.png";
    }

    newRecipe.imagem = imgUrl;

    // nome
    newRecipe.nome = document.querySelector('.rec-nome').value;

    // ingredientes
    newRecipe.ingredientes = [];
    document.querySelectorAll('.input-g.ingredientes input').forEach(el => {
        newRecipe.ingredientes.push(el.value);
    })

    // preparo
    newRecipe.preparo = [];
    document.querySelectorAll('.input-g.preparo input').forEach(el => {
        newRecipe.preparo.push(el.value);
    })

    //joga pro firebase
    userRef.collection('recipes').add(newRecipe);

    //limpa o form e fecha
    let imgStorage = document.querySelector('.imagem-storage');
    if (imgStorage) {
        let parent = imgStorage.parentElement;

        parent.innerHTML = '';

        //colocar o input de volta
        let fileI = document.createElement('input');
        fileI.setAttribute('type', 'file');
        fileI.setAttribute('accept', 'image/*');
        fileI.setAttribute('onchange', 'uploadImage(event)');
        fileI.setAttribute('name', 'file');
        fileI.setAttribute('id', 'file');
        fileI.classList.add('inputfile');

        let labelI = document.createElement('label');
        labelI.setAttribute('for', 'file');
        labelI.innerHTML = "Escolher imagem";

        parent.appendChild(fileI);
        parent.appendChild(labelI);
    }

    document.querySelector('.rec-nome').value = '';

    let ings = document.querySelector('.input-g.ingredientes');
    while (ings.childElementCount > 3) {
        ings.removeChild(ings.children[2]);
    }
    ings.querySelector('input').value = '';

    let prep = document.querySelector('.input-g.preparo');
    while (prep.childElementCount > 3) {
        prep.removeChild(prep.children[2]);
    }
    prep.querySelector('input').value = '';

    toggleAddPannel();
}